package com.test.swapsharma.techchallenge.di.component;

import com.test.swapsharma.techchallenge.di.module.ActivityModule;
import com.test.swapsharma.techchallenge.di.scope.PerActivity;
import com.test.swapsharma.techchallenge.ui.OpenJobsFragment;

import dagger.Component;

/**
 * A @Component interface defines the connection between provider of objects (modules) and the objects which expresses a
 * dependency.
 * <p>
 * Dagger 2 uses this interface to generate the accessor class which provides the methods defined in the interface.
 * <p>
 * In this class we define from which modules (or other Components) we are taking dependencies.
 * <p>
 * Also here we define which graph dependencies should be visible publicly (can be injected)
 * and where our component can inject objects.
 * <p>
 * * <p>
 * The base pattern for the generated class is that Dagger is used as prefix followed by the interface name.
 * The generated class has a create method which allows configuring the objects based on the given configuration.
 * The methods defined on this interface are available to access the generated objects.
 * <p>
 * <p>
 * This component injects dependencies to all Activities/fragments across the application
 * <p>
 **/
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent
{
	// allow to inject into our Jobs fragment class
	void inject(OpenJobsFragment openJobsFragment);
}
