package com.test.swapsharma.techchallenge.ui;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.swapsharma.techchallenge.R;
import com.test.swapsharma.techchallenge.domain.Job;
import com.test.swapsharma.techchallenge.domain.JobsResponse;
import com.test.swapsharma.techchallenge.presenter.JobsPresenterInterface;
import com.test.swapsharma.techchallenge.presenter.OpenJobsView;
import com.test.swapsharma.techchallenge.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenJobsFragment extends BaseFragment implements OpenJobsView
{
	@BindView(R.id.recycler_view) protected RecyclerView jobsRv;
	@Inject
	JobsPresenterInterface<OpenJobsView> mPresenter;
	
	public OpenJobsFragment() {
		// Required empty public constructor
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_open_jobs, container, false);
		ButterKnife.bind(this, view);
		getActivityComponent().inject(this);
		mPresenter.onAttach(this);
		mPresenter.fetchJobs();
		return view;
	}

	@Override
	public void showJobsAsList(JobsResponse jobsResponse)
	{
		this.setUpRecyclerView(jobsResponse.getJobs());
	}
	/**
	 * This method sets up the custom recycler view. We call the webservice and get data here from  API and
	 * then we need to set it RV.
	 */
	private void setUpRecyclerView(List<Job> jobList)
	{
		JobsAdapter adapter = new JobsAdapter(getActivity(), jobList);
		jobsRv.setHasFixedSize(true);
		jobsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
		jobsRv.setItemAnimator(new DefaultItemAnimator());
		jobsRv.setAdapter(adapter);
	}
}
