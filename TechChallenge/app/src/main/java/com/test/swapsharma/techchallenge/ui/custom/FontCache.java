package com.test.swapsharma.techchallenge.ui.custom;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontCache
{
	private static Map<String, Typeface> fontCache = new HashMap<>();

	public static Typeface getTypeface(String fontname, Context context)
	{
		Typeface typeface = fontCache.get(fontname);

		if (typeface == null)
		{
			try
			{
				typeface = Typeface.createFromAsset(context.getAssets(), fontname);
			} catch (Exception e)
			{
				return null;
			}

			fontCache.put(fontname, typeface);
		}

		return typeface;
	}
}
