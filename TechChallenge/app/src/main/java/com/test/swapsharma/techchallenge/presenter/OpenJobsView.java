package com.test.swapsharma.techchallenge.presenter;


import com.test.swapsharma.techchallenge.domain.JobsResponse;
import com.test.swapsharma.techchallenge.ui.base.BaseView;

/**
 * JobsActivity view interface.
 */
public interface OpenJobsView extends BaseView
{
	void showJobsAsList(JobsResponse jobsResponse);
}
