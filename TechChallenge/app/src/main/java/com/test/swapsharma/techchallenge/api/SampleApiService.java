package com.test.swapsharma.techchallenge.api;

import com.test.swapsharma.techchallenge.domain.JobsResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * This class manages the network API calls and API data handling.
 */
@Singleton
public class SampleApiService implements SampleApi
{
	@Inject
	public SampleApiService()
	{//required for injection
	}

	@Override
	public Observable<JobsResponse> getJobsFromServer()
	{
		return RetrofitBuilder.createApiService().getJobsFromServer();
	}
}
