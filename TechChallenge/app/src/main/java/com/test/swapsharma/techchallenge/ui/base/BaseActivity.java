package com.test.swapsharma.techchallenge.ui.base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.test.swapsharma.techchallenge.SampleApplication;
import com.test.swapsharma.techchallenge.di.component.ActivityComponent;
import com.test.swapsharma.techchallenge.di.component.DaggerActivityComponent;
import com.test.swapsharma.techchallenge.di.module.ActivityModule;


/**
 * Base Activity for all the activities in the app.
 * This is the mother of all activities and takes care of showing toasts, Progress bars, snack-bars, custom alerts
 * etc.
 * All activities must extend this activity.
 * This is an abstract class and you have full control over its methods.
 * <p>
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView,BaseFragment.Callback
{
	private ActivityComponent mActivityComponent;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		mActivityComponent = DaggerActivityComponent.builder()
				.activityModule(new ActivityModule(this))
				.applicationComponent(((SampleApplication) getApplication()).getComponent())
				.build();
	}
	
	@Override
	public void showProgress()
	{
		Toast.makeText(this, "Showing Progress bar ", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void hideProgress()
	{
		Toast.makeText(this, "Hiding progress bar", Toast.LENGTH_SHORT).show();
	}
	

	@Override
	public void onError(String message)
	{
		Toast.makeText(this, "error" + message, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * This method is responsible to let our app know if their is an active data connection avaialble on the device or not.
	 * It returns true in case the device has an active Wi-Fi or 3G/4G connection.
	 *
	 * @param context
	 * @return a boolean confirming if network is available or not.
	 */
	protected boolean isInternetAvailable(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return context != null && activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	protected void updateProgressBar(boolean shouldShow)
	{
//		ProgressBar progressView = (ProgressBar) findViewById(R.id.progressBar);
//		if (shouldShow)
//		{
//			if (null != progressView)
//			{
//				progressView.setVisibility(View.VISIBLE);
//			}
//		}
//		else
//		{
//			if (null != progressView)
//			{
//				progressView.setVisibility(View.GONE);
//			}
//		}
	}
	public ActivityComponent getActivityComponent() {
		return mActivityComponent;
	}
	
	/**
	 * This method is for setting up generic UI components like Toolbars, Navigation views etc inside the activities.
	 */
	protected abstract void setUpGenericUI();
	
	@Override
	public void onFragmentAttached() {
		
	}
}
