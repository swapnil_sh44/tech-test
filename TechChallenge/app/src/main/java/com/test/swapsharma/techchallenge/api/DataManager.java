package com.test.swapsharma.techchallenge.api;

/**
 * This is an interface that is implemented by the SampleDataManager and it delegates the services provided by all the Helper
 * classes.
 * <p>
 * For this DataManager interface extends Helper interfaces
 * <p>
 * The Model of our MVP is broken into helpers like :
 * <p>
 * SampleApi - for API operations
 * <p>
 * PreferenceHelper - for shared preferences
 * <p>
 * FileHelper- to interact with files ex. giving offline support feature using local json file.
 * <p>
 * The DataManager class here extends these helpers, and binds all Models.
 * <p>
 * Various presenters in the app communicate with the DataManager through an interface
 */
public interface DataManager extends SampleApi
{
	void setDataFromAPI();
	
}
