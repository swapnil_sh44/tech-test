package com.test.swapsharma.techchallenge.presenter;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.test.swapsharma.techchallenge.api.DataManager;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * This class is the decision-making counterpart of the JobsActivity.
 * This is is a pure java class, with no access to Android APIs.
 * <p>
 * This Presenter receives the user interactions passed on from JobsActivity/Fragment(or child views) and then takes the
 * decision, based on the business logic, and then instructs the View to perform specific actions.
 * <p>
 * It also communicates with the DataManager for any data it needs to perform business logic.
 * <p>
 * This presenter - JobsPresenterImpl is responsible to fetch the data from server for open Jobs screen and then sets the data
 * or performs additional operations and also intimates the UI about the handling errors.
 *
 * @param <V>
 */
public class JobsPresenterImpl<V extends OpenJobsView> extends BasePresenter<V>
		implements JobsPresenterInterface<V>
{
	private static final String TAG = JobsPresenterImpl.class.getSimpleName();
	
	@Inject
	public JobsPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable)
	{
		super(dataManager, compositeDisposable);
	}
	
	/**
	 * This method is to fetch jobs
	 */
	@Override
	public void fetchJobs()
	{
		getMvpView().showProgress();
		mCompositeDisposable.add(mDataManager.getJobsFromServer().
				observeOn(AndroidSchedulers.mainThread())
				                         .subscribe(jobsResponse ->
				                                    {
					                                    if (!JobsPresenterImpl.this.isViewAttached())
					                                    {
						                                    return;
					                                    }
					                                    if (jobsResponse != null)
					                                    {
						                                    getMvpView().showJobsAsList(jobsResponse);
					                                    }
				                                    }, throwable ->
				                                    {
					                                    if (!isViewAttached())
					                                    {
						                                    return;
					                                    }
					                                    if (throwable instanceof HttpException)
					                                    {
						                                    int errorCode = ((HttpException) throwable).code();
						                                    JobsPresenterImpl.this.handleApiError(errorCode);
					                                    }
					                                    else
					                                    {
						                                    //send exception to generic/other exception handler method in base
						                                    System.out.println(":::::::Exception:::::"+ throwable.getMessage
								                                    ());
					                                    }
				                                    }
				                         ));
	}
}
