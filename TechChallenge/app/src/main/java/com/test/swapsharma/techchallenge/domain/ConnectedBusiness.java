package com.test.swapsharma.techchallenge.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConnectedBusiness
{
	
	@SerializedName("businessId")
	@Expose
	private Integer businessId;
	@SerializedName("thumbnail")
	@Expose
	private String thumbnail;
	@SerializedName("isHired")
	@Expose
	private Boolean isHired;
	
	public Integer getBusinessId()
	{
		return businessId;
	}
	
	public void setBusinessId(Integer businessId)
	{
		this.businessId = businessId;
	}
	
	public String getThumbnail()
	{
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail)
	{
		this.thumbnail = thumbnail;
	}
	
	public Boolean getIsHired()
	{
		return isHired;
	}
	
	public void setIsHired(Boolean isHired)
	{
		this.isHired = isHired;
	}
	
	@Override
	public String toString()
	{
		return "ConnectedBusiness{" +
				"businessId=" + businessId +
				", thumbnail='" + thumbnail + '\'' +
				", isHired=" + isHired +
				'}';
	}
}
