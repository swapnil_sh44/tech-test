package com.test.swapsharma.techchallenge.api;

import com.test.swapsharma.techchallenge.domain.JobsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * An interface for defining methods to be used for making network calls using retrofit.
 * It is an interface having all the methods for making API calls.
 * <p>
 * Retrofit favors composition over inheritance. One interface per service.All methods for API to reside here.
 */
public interface SampleApi
{
	/**
	 * This method gets the jobs from the server and returns a observable of JobsResponse type.
	 * This is a GET method.
	 * @return Observable<JobsResponse>
	 */
	@GET("jobs.json")
	Observable<JobsResponse> getJobsFromServer();
}
