package com.test.swapsharma.techchallenge;


import android.app.Application;

import com.test.swapsharma.techchallenge.api.DataManager;
import com.test.swapsharma.techchallenge.di.component.ApplicationComponent;
import com.test.swapsharma.techchallenge.di.component.DaggerApplicationComponent;
import com.test.swapsharma.techchallenge.di.module.ApplicationModule;

import javax.inject.Inject;

public class SampleApplication extends Application
{
	private static ApplicationComponent mApplicationComponent;
	@Inject
	DataManager mDataManager;

	public static ApplicationComponent getComponent()
	{
		return mApplicationComponent;
	}
	
	public void onCreate()
	{
		super.onCreate();
		mApplicationComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this)).build();

		mApplicationComponent.inject(this);
	}
}
