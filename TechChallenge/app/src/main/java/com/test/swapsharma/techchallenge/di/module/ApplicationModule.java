package com.test.swapsharma.techchallenge.di.module;

import android.app.Application;
import android.content.Context;

import com.test.swapsharma.techchallenge.api.DataManager;
import com.test.swapsharma.techchallenge.api.SampleApi;
import com.test.swapsharma.techchallenge.api.SampleApiService;
import com.test.swapsharma.techchallenge.api.SampleDataManager;
import com.test.swapsharma.techchallenge.di.qualifiers.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This class is a module class and returns objects for Dependency Injection.
 */
@Module
public class ApplicationModule
{
	private final Application mApplication;
	
	public ApplicationModule(Application application)
	{
		mApplication = application;
	}
	
	@Provides
	@ApplicationContext
	Context provideContext()
	{
		return mApplication;
	}
	
	@Provides
	Application provideApplication()
	{
		return mApplication;
	}
	
	// Single instance of this provided object is created and shared.
	@Provides
	@Singleton
	DataManager provideDataManager(SampleDataManager appDataManager)
	{
		return appDataManager;
	}
	
	@Provides
	@Singleton
	SampleApi provideSampleApi(SampleApiService sampleApiService)
	{
		return sampleApiService;
	}
}
