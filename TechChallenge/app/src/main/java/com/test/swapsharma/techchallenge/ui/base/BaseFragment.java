package com.test.swapsharma.techchallenge.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.test.swapsharma.techchallenge.di.component.ActivityComponent;


/**
 * A simple {@link Fragment} base class.
 * <p>
 * Base Fragment class for all the fragments.
 * This class takes care of common operations like error Handling, showing toasts, Progress bars, snack-bars, custom
 * alerts etc.
 * All Fragments must extend this class.
 * This is an abstract class and you have full control over its methods.
 * <p>
 */
public class BaseFragment extends Fragment implements BaseView
{
	private BaseActivity mActivity;
	
	public BaseFragment()
	{
		// Required empty public constructor
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof BaseActivity) {
			BaseActivity activity = (BaseActivity) context;
			this.mActivity = activity;
			activity.onFragmentAttached();
		}
	}
	
	@Override
	public void onDetach() {
		mActivity = null;
		super.onDetach();
	}
	
	protected ActivityComponent getActivityComponent() {
		return mActivity.getActivityComponent();
	}
	
	public BaseActivity getBaseActivity() {
		return mActivity;
	}

	@Override
	public void showProgress()
	{
		Toast.makeText(getActivity(), "Showing Progress bar ", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void hideProgress()
	{
		Toast.makeText(getActivity(), "Hiding progress bar", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onError(String message)
	{
		Toast.makeText(getActivity(), "error" + message, Toast.LENGTH_SHORT).show();
	}
	
	public interface Callback {
		void onFragmentAttached();
		void onFragmentDetached(String tag);
	}
}
