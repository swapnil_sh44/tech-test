package com.test.swapsharma.techchallenge.di.module;

import android.app.Activity;
import android.content.Context;

import com.test.swapsharma.techchallenge.di.qualifiers.ActivityContext;
import com.test.swapsharma.techchallenge.di.scope.PerActivity;
import com.test.swapsharma.techchallenge.presenter.JobsPresenterImpl;
import com.test.swapsharma.techchallenge.presenter.JobsPresenterInterface;
import com.test.swapsharma.techchallenge.presenter.OpenJobsView;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * @Module classes are responsible for providing objects which can be injected. These classes can define methods annotated with
 * @Provides and the returned objects from these methods are available for dependency injection.
 * This class provides presenter objects for DI.
 */
@Module
public class ActivityModule
{
	private Activity mActivity;
	public ActivityModule(Activity activity)
	{
		this.mActivity = activity;
	}

	@Provides
	@ActivityContext
	Context provideContext()
	{
		return mActivity;
	}

	@Provides
	CompositeDisposable provideCompositeDisposable()
	{
		return new CompositeDisposable();
	}

	@Provides
	Activity provideActivity()
	{
		return mActivity;
	}

	@Provides
	@PerActivity
	JobsPresenterInterface<OpenJobsView> provideJobsPresenter(JobsPresenterImpl<OpenJobsView>
			                                                      presenter)
	{
		return presenter;
	}
}
