package com.test.swapsharma.techchallenge.presenter;

import com.test.swapsharma.techchallenge.ui.base.BaseView;

/**
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the MvpView type that wants to be attached with.
 */
public interface BasePresenterInterface<V extends BaseView>
{
	
	void onAttach(V mvpView);
	
	void onDetach();
	
	void handleApiError(int errorCode);
	
}
