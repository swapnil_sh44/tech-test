package com.test.swapsharma.techchallenge.ui.base;

/**
 * A Base interface that any class that wants to act as a View in the MVP pattern must implement. Generally this interface
 * will be extended by a more specific interface, which in turn will usually be implemented by an Activity or Fragment.
 * <p>
 * It should be implemented by all the vies. It contains basic methods that are exposed to its Presenter for the
 * communication and the implementations can have feature specific methods.
 */
public interface BaseView
{
	void showProgress();
	
	void hideProgress();
	
	void onError(String message);
}
