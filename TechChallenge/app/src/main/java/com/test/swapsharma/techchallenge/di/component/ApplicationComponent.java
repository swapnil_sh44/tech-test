package com.test.swapsharma.techchallenge.di.component;

import android.content.Context;

import com.test.swapsharma.techchallenge.SampleApplication;
import com.test.swapsharma.techchallenge.api.DataManager;
import com.test.swapsharma.techchallenge.di.module.ApplicationModule;
import com.test.swapsharma.techchallenge.di.qualifiers.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A @Component interface defines the connection between provider of objects (modules) and the objects which expresses a
 * dependency.
 * <p>
 * Dagger 2 uses this interface to generate the accessor class which provides the methods defined in the interface.
 * <p>
 * In this class we define from which modules (or other Components) we are taking dependencies.
 * <p>
 * Also here we define which graph dependencies should be visible publicly (can be injected)
 * and where our component can inject objects.
 * <p>
 * * <p>
 * The base pattern for the generated class is that Dagger is used as prefix followed by the interface name.
 * The generated class has a create method which allows configuring the objects based on the given configuration.
 * The methods defined on this interface are available to access the generated objects.
 * <p>
 **/
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent
{
	void inject(SampleApplication app);
	@ApplicationContext
	Context context();
	//Application application();
	// provide the dependency for dependent components
	DataManager getDataManager();
}
