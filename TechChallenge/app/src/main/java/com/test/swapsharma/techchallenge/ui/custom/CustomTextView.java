package com.test.swapsharma.techchallenge.ui.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


public class CustomTextView extends AppCompatTextView
{
	public CustomTextView(Context context)
	{
		super(context);

		this.applyCustomFont(context);
	}

	public CustomTextView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		this.applyCustomFont(context);
	}

	public CustomTextView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		this.applyCustomFont(context);
	}

	private void applyCustomFont(Context context)
	{
		Typeface customFont = FontCache.getTypeface("fonts/source-sans-pro.regular.ttf", context);
		setTypeface(customFont);
	}
}
