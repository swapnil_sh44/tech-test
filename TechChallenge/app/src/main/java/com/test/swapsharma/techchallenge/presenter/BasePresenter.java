package com.test.swapsharma.techchallenge.presenter;

import android.util.Log;

import com.google.gson.JsonSyntaxException;
import com.test.swapsharma.techchallenge.api.DataManager;
import com.test.swapsharma.techchallenge.ui.base.BaseView;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import io.reactivex.disposables.CompositeDisposable;

/**
 * This is a  Base class that implements the BasePresenterInterface and provides a base implementation for
 * onAttach() and onDetach(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 * <p>
 * None of the presenter have access to any of the Android’s apis and can be tested independently
 * of the android APIs.
 * <p>
 * There is a one-to-one relationship which exists between the view and Presenter here.
 * <p>
 * Hence we manage to get a loose coupling between the implementation from the usage.
 * <p>
 * Depending on our requirement we can at any time attach a new presenter to an
 * fragment or activity that implements the same PresenterInterface without breaking/modifying any other
 * component or module or baseness logic in other modules of the app.
 */
public class BasePresenter<V extends BaseView> implements BasePresenterInterface<V>
{
	private static final String TAG = "BasePresenter";
	protected final CompositeDisposable mCompositeDisposable;
	protected final DataManager mDataManager;
	private V mMvpView;
	
	@Inject
	protected BasePresenter(DataManager dataManager, CompositeDisposable compositeDisposable)
	{
		this.mDataManager = dataManager;
		this.mCompositeDisposable = compositeDisposable;
	}
	
	@Override
	public void onAttach(V mvpView)
	{
		mMvpView = mvpView;
	}
	
	@Override
	public void onDetach()
	{
		mCompositeDisposable.dispose();
		mMvpView = null;
	}
	
	@Override
	public void handleApiError(int errorCode)
	{
		try
		{
			System.out.println("::::::API Error code:::::" + errorCode);
			//401
			if (errorCode == HttpsURLConnection.HTTP_UNAUTHORIZED)
			{
				getMvpView().onError("Auth error - 401 ");
				return;
			}
			//400
			else if (errorCode == HttpsURLConnection.HTTP_BAD_REQUEST)
			{
				getMvpView().onError("Error - 400");
				return;
			}
			//403
			else if (errorCode == HttpsURLConnection.HTTP_FORBIDDEN)
			{
				getMvpView().onError("Error - 403 ");
				return;
			}
			//404
			else if (errorCode == HttpsURLConnection.HTTP_NOT_FOUND)
			{
				getMvpView().onError("Error - 404");
				return;
			}
			//500
			else if (errorCode == HttpsURLConnection.HTTP_INTERNAL_ERROR)
			{
				getMvpView().onError("Error - 500");
				return;
			}
			else
			{
				getMvpView().onError("Some error!, Please try again later");
				return;
			}
		} catch (JsonSyntaxException | NullPointerException e)
		{
			Log.e(TAG, "handleApiError", e);
			getMvpView().onError("API error");
		}
	}
	
	/**
	 * This method keeps a reference to the mvpView that
	 * can be accessed from the children classes by calling getMvpView() method.
	 * <p>
	 * Naming it as getMvpView() instead of getView() or getBaseView() or getParentView() to avoid confusion.
	 *
	 * @return MvpView
	 */
	protected V getMvpView()
	{
		return mMvpView;
	}
	
	public void checkViewAttached()
	{
		if (!isViewAttached())
		{
			throw new MvpViewNotAttachedException();
		}
	}
	
	protected boolean isViewAttached()
	{
		return mMvpView != null;
	}
	
	public static class MvpViewNotAttachedException extends RuntimeException
	{
		public MvpViewNotAttachedException()
		{
			super("Please call Presenter.onAttach(MvpView) before" +
					      " requesting data to the Presenter");
		}
	}
}
