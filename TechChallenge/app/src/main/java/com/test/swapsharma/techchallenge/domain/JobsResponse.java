package com.test.swapsharma.techchallenge.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobsResponse
{
	@SerializedName("jobs")
    @Expose
    private List<Job> jobs = null;
	
	public List<Job> getJobs() {
		return jobs;
	}
	
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}
	
}
