package com.test.swapsharma.techchallenge.presenter;


import com.test.swapsharma.techchallenge.di.scope.PerActivity;

/**
 * An interface with method signatures to be utilized in implementing business logic for JobsActivity.
 *
 * @param <V> any view extending OpenJobsView
 */
@PerActivity
public interface JobsPresenterInterface<V extends OpenJobsView> extends BasePresenterInterface<V>
{
	void fetchJobs();
}
