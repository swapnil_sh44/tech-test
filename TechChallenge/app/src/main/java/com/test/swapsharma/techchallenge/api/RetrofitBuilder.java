package com.test.swapsharma.techchallenge.api;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.test.swapsharma.techchallenge.BuildConfig;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * This class provides a retrofit instance for making the network calls using okHttp 3 client.
 */
class RetrofitBuilder
{
	
	private RetrofitBuilder()
	{
		//empty constructor
	}
	
	/**
	 * Build a new {@link Retrofit} object
	 * <p>
	 * Calling baseUrl  is required before calling build(). All other methods are optional.
	 * Create an implementation of the API endpoints defined by the service interface.
	 */
	static SampleApi createApiService()
	{
		String baseUrl=	BuildConfig.BASE_URL;
		
		OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
		if (BuildConfig.DEBUG)
		{
			HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
			logging.setLevel(HttpLoggingInterceptor.Level.BODY);
			okHttpClientBuilder.addInterceptor(logging);
		}
		okHttpClientBuilder.interceptors().add(chain ->
		                                       {
			                                       Request request = chain.request();
			                                       Log.d("OkHttp REQUEST", request.toString());
			                                       Response response = chain.proceed(request);
			                                       response = response.newBuilder().build();
			                                       Log.d("OkHttp RESPONSE", response.toString());
			                                       return response;
		                                       });
		
		OkHttpClient okHttpClient = okHttpClientBuilder.build();
		Retrofit.Builder builder =
				new Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
						.addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
						.baseUrl(baseUrl);
		builder.client(okHttpClient);
		return builder.build().create(SampleApi.class);
	}
}
