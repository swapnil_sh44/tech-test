package com.test.swapsharma.techchallenge.utils;

public class DateUtils
{
	
	public static String getDayOfMonthSuffix(int dayOfMonth )
	{
		switch (dayOfMonth)
		{
			case 1:
			case 21:
			case 31:
				return "st";
			case 2:
			case 22:
				return "nd";
			case 3:
			case 23:
				return "rd";
			default:
				return "th";
		}
	}
	
	
}
