package com.test.swapsharma.techchallenge.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Job
{
	@SerializedName("jobId")
    @Expose
    private Integer jobId;
	@SerializedName("category")
	@Expose
	private String category;
	@SerializedName("postedDate")
	@Expose
	private String postedDate;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("connectedBusinesses")
	@Expose
	private List<ConnectedBusiness> connectedBusinesses= null;
	@SerializedName("detailsLink")
	@Expose
	private String detailsLink;
	
	public Integer getJobId() {
		return jobId;
	}
	
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getPostedDate() {
		return postedDate;
	}
	
	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<ConnectedBusiness> getConnectedBusinesses() {
		return connectedBusinesses;
	}
	
	public void setConnectedBusinesses(List<ConnectedBusiness> connectedBusinesses) {
		this.connectedBusinesses = connectedBusinesses;
	}
	
	public String getDetailsLink() {
		return detailsLink;
	}
	
	public void setDetailsLink(String detailsLink) {
		this.detailsLink = detailsLink;
	}
	
}
