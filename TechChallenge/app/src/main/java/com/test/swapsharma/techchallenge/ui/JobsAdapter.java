package com.test.swapsharma.techchallenge.ui;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.squareup.picasso.Picasso;
import com.test.swapsharma.techchallenge.R;
import com.test.swapsharma.techchallenge.domain.Job;
import com.test.swapsharma.techchallenge.ui.custom.CustomTextView;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.test.swapsharma.techchallenge.utils.DateUtils.getDayOfMonthSuffix;

/**
 * An adapter class for Recycler view displayed in the the Open Jobs fragment.
 */
public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.MyViewHolder>
{
	private Context mContext;
	private List<Job> jobList;
	
	public JobsAdapter(Context mContext, List<Job> jobList) {
		this.mContext = mContext;
		this.jobList = jobList;
	}
	
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.job_card, parent, false);
		return new MyViewHolder(itemView);
	}
	
	@Override
	public void onBindViewHolder(final MyViewHolder holder, int position) {
		Job jobObject = jobList.get(position);
		holder.categoryTv.setText(jobObject.getCategory());
		
		String stringDate = jobObject.getPostedDate();
		DateTime dateTime = DateTime.parse(stringDate, DateTimeFormat.forPattern("yyyy-MM-dd"));
		
		String formattedDate= dateTime.dayOfMonth().getAsString()+
				getDayOfMonthSuffix(Integer.parseInt(dateTime.dayOfMonth().getAsString()))+" "+
				dateTime.monthOfYear().getAsText()+" "+dateTime.year().getAsString();
		
		holder.postedOnTv.setText("Posted: "+ formattedDate);
		
		holder.statusTv.setText(jobObject.getStatus());
		if (jobObject.getConnectedBusinesses()!= null)
		{
			LinearLayout.LayoutParams singleBusinessLayoutParams = new LinearLayout.LayoutParams(100, 120);
			singleBusinessLayoutParams.gravity= Gravity.CENTER;
			
			LinearLayout.LayoutParams circularThumbnailIvLayoutParams = new LinearLayout.LayoutParams(90, 90);
			circularThumbnailIvLayoutParams.gravity= Gravity.CENTER;
			
			LinearLayout.LayoutParams hiringStatusLayoutParams = new LinearLayout.LayoutParams(60, 25);
			hiringStatusLayoutParams.gravity= Gravity.CENTER;
			
			LinearLayout.LayoutParams textViewLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams
					                                                                               .WRAP_CONTENT,
			                                                                               ViewGroup.LayoutParams.WRAP_CONTENT);
			textViewLayoutParams.gravity= Gravity.CENTER;
			
			
			if (jobObject.getConnectedBusinesses().size() > 0)
			{
				int hiredCount = 0;
				for (int i = 0; i < jobObject.getConnectedBusinesses().size(); i++)
				{
					LinearLayout singleBusinessLayout = new LinearLayout(mContext);
					singleBusinessLayout.setOrientation(LinearLayout.VERTICAL);
					singleBusinessLayout.setClipChildren(false);
					
					ImageView circularThumbnailIv = new ImageView(mContext);
					circularThumbnailIv.setPadding(4,4,4,4);
					circularThumbnailIv.setLayoutParams(circularThumbnailIvLayoutParams);

					// loading using Picasso library
					Picasso.with(mContext).load(jobObject.getConnectedBusinesses().get(i).getThumbnail()).fit().into(circularThumbnailIv);
					singleBusinessLayout.addView(circularThumbnailIv);
					
					if (jobObject.getConnectedBusinesses().get(i).getIsHired())
					{
						hiredCount++;
						LinearLayout hiringStatusLayout = new LinearLayout(mContext);
						hiringStatusLayout.setLayoutParams(hiringStatusLayoutParams);
						hiringStatusLayoutParams.setMargins(0,-15,0,0);
						hiringStatusLayout.setBackgroundColor(Color.parseColor("#FF8C00"));
						hiringStatusLayout.setBackgroundResource(R.drawable.corners_rounded);
						
						TextView textView = new TextView(mContext);
						textView.setText("HIRED");
						textView.setTextSize(14);
						
						textView.setLayoutParams(textViewLayoutParams);
						textView.setPadding(8,2,8,2);
						textView.setTextColor(Color.parseColor("#FFFFFF"));
						
						hiringStatusLayout.addView(textView);
						
						singleBusinessLayout.setLayoutParams(singleBusinessLayoutParams);
						singleBusinessLayout.addView(hiringStatusLayout);
					}
				else {
						singleBusinessLayout.setLayoutParams(singleBusinessLayoutParams);
					}
					holder.imageContainer.addView(singleBusinessLayout);
				}
				holder.hiredInfoTv.setText("You have hired"+hiredCount+"buisenesses");
			}
		}
		else {
			
			holder.imageContainer.setVisibility(View.GONE);
		}
		holder.overflow.setOnClickListener(view -> showPopupMenu(holder.overflow));
	}
	
	/**
	 * Showing popup menu when tapping on the three dots, mimicing an overflow menu
	 */
	private void showPopupMenu(View view) {
		// inflate menu
		PopupMenu popup = new PopupMenu(mContext, view);
		MenuInflater inflater = popup.getMenuInflater();
		inflater.inflate(R.menu.job_card_menu, popup.getMenu());
		popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
		popup.show();
	}

	@Override
	public int getItemCount() {
		return jobList.size();
	}
	
	
	class MyViewHolder extends RecyclerView.ViewHolder {
		CustomTextView categoryTv;
		CustomTextView postedOnTv;
		CustomTextView statusTv,hiredInfoTv;
		LinearLayout imageContainer;
		ImageView  overflow;
		MyViewHolder(View view) {
			super(view);
			categoryTv = (CustomTextView) view.findViewById(R.id.categoryTv);
			postedOnTv = (CustomTextView) view.findViewById(R.id.postedOnTv);
			statusTv = (CustomTextView) view.findViewById(R.id.statusTv);
			hiredInfoTv = (CustomTextView) view.findViewById(R.id.hiredInfoTv);
			overflow = (ImageView) view.findViewById(R.id.overflow);
			imageContainer = (LinearLayout) view.findViewById(R.id.imageContainer);
		}
	}
	/**
	 * Click listener for popup menu items
	 */
	class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
		
		public MyMenuItemClickListener() {
		}
		
		@Override
		public boolean onMenuItemClick(MenuItem menuItem) {
			switch (menuItem.getItemId()) {
				case R.id.action_close:
					Toast.makeText(mContext, "Close Job", Toast.LENGTH_SHORT).show();
					return true;
	
				default:
			}
			return false;
		}
	}
}
