package com.test.swapsharma.techchallenge.api;


import android.content.Context;

import com.test.swapsharma.techchallenge.di.qualifiers.ApplicationContext;
import com.test.swapsharma.techchallenge.domain.JobsResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


/**
 * This class implements DataManager interface and is the one single of contact for any data related operation.
 * Helpers like ApiHelper, PreferenceHelper, and File Helper only work for DataManager.
 * This delegates/routes all the operations specific to any Helper.
 * <p>
 * This exists as a singleton instance.
 */
@Singleton
public class SampleDataManager implements DataManager
{
	private static final String TAG = "SampleDataManager";
	private final Context mContext;
	private final SampleApi mSampleApi;
	
	@Inject
	public SampleDataManager(@ApplicationContext Context context, SampleApi sampleApi)
	{
		mContext = context;
		mSampleApi = sampleApi;
	}
	
	@Override
	public void setDataFromAPI()
	{
		//Todo
	}

	@Override
	public Observable<JobsResponse> getJobsFromServer()
	{
		return mSampleApi.getJobsFromServer();
	}
	
}
