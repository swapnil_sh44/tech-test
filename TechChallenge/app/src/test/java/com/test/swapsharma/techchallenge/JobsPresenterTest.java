package com.test.swapsharma.techchallenge;

import com.test.swapsharma.techchallenge.api.DataManager;
import com.test.swapsharma.techchallenge.domain.JobsResponse;
import com.test.swapsharma.techchallenge.presenter.JobsPresenterImpl;
import com.test.swapsharma.techchallenge.presenter.OpenJobsView;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class JobsPresenterTest
{
	@Mock
	OpenJobsView mMockView;
	@Mock
	DataManager mMockDataManager;
	private JobsPresenterImpl<OpenJobsView> mMockPresenter;
	
	//setup for Rx to be run once before any of the test methods in the class will run.
	//Create and return a Scheduler that queues work on the current thread to be executed after the current work completes.
	@BeforeClass
	public static void setUpRx() throws Exception
	{
		RxAndroidPlugins.setInitMainThreadSchedulerHandler(
				new Function<Callable<Scheduler>, Scheduler>()
				{
					@Override
					public Scheduler apply(Callable<Scheduler> schedulerCallable)
							throws Exception
					{
						return Schedulers.trampoline();
					}
				});
	}
	
	//set up compositeDisposable, data manager and presenter here, attach view.
	@Before
	public void setUpJobsPresenterTest() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		CompositeDisposable compositeDisposable = new CompositeDisposable();
		mMockPresenter = new JobsPresenterImpl<>(mMockDataManager, compositeDisposable);
		mMockPresenter.onAttach(mMockView);
	}
	
	@Test
	public void testApiForJobs()
	{
		JobsResponse jobsResponse = new JobsResponse();
		doReturn(Observable.just(jobsResponse))
				.when(mMockDataManager).getJobsFromServer();
		mMockPresenter.fetchJobs();
	}
	
	//  detach disposable and view and removes all handlers and resets the default behavior.
	@After
	public void finishEveryThingPostTest() throws Exception
	{
		mMockPresenter.onDetach();
		RxAndroidPlugins.reset();
	}
}
